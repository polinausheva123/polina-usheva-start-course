var config = {
   entry: './main.js',

   output: {
      path:'./',
      filename: 'build/bundle.js',
   },

   devServer: {
      inline: true,
      port: 8080
   },

   watchOptions: {
       poll: true
   },

   module: {
      loaders: [
         {
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel',

            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   }
}

module.exports = config;
